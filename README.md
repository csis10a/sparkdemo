# sparkdemo

This is an example Maven project that contains a few simple demos for the [Spark](http://www.sparkjava.com/) micro web framework.

## Demo

### HelloWorld
```
mvn compile
mvn exec:java -Dexec.mainClass=csis10a.sparkdemo.HelloWorld
```

- Open a browser to http://localhost:4567/hello
- When you are done press [ctrl]+c in the terminal window


### NameEcho
```
mvn compile
mvn exec:java -Dexec.mainClass=csis10a.sparkdemo.NameEcho
```

- Open a browser to http://localhost:4567
- Change the URL to http://localhost:4567/name/Your Name
- When you are done press [ctrl]+c in the terminal window

### FreeMarkerExample
[FreeMarker](http://freemarker.org/) is an HTML templating langauge that integrates well with Spark. To run an example using FreeMarker:

```
mvn compile
mvn exex:java -Dexec.mainClass=csis10a.sparkdemo.FreeMarkerExample
```
- Open a browser to http://localhost:4567/hello
- In the browser, view the source code for the page. Compare this to the FreeMarker template that was used for this page in `src\resources\spark\templates\freemarker\hello.ftl`.
- When you are done press [ctrl]+c in the terminal window