package csis10a.sparkdemo;

import spark.Spark;
import spark.*;

public class NameEcho {
  public static void main(String[] args) {
    
    Spark.get(new Route("/") {
      @Override
      public Object handle(Request request, Response response) {
        response.redirect("/name/you forgot a name");
        return null;
      }
    });
    
    Spark.get(new Route("/name/:name") {
      @Override
      public Object handle(Request request, Response response) {
        String name = request.params(":name");
        name = name.replace("%20", " ");
        return "Hello " + name;
      }
    });

  }
}